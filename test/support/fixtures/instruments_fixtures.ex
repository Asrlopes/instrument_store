defmodule InstrumentStore.InstrumentsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `InstrumentStore.Instruments` context.
  """

  @doc """
  Generate a guitar.
  """
  def guitar_fixture(attrs \\ %{}) do
    {:ok, guitar} =
      attrs
      |> Enum.into(%{
        is_available: true,
        model: "some model",
        year: 42
      })
      |> InstrumentStore.Instruments.create_guitar()

    guitar
  end
end
